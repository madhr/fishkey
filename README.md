# FishKey

Flashcards application made with Spring Boot REST API and Angular2.

## Getting Started

There are actually two projects. First one is a RESTful web service, built with Spring. The second one is an Angular2 project, and it has been placed under
 ```
 \src\main\frontend
 ```
 Since these projects are independent, they can put into different locations. I've decided to keep them together, as this made it easier for me to develop the application.

A proxy for API calls has been configured with Angular CLI. See *proxy-conf.json* file.



### Prerequisites

To work with this project, you should install the following:
* Java 8
* Maven (at least 3.3.3)
* npm (at least 3.x.x)
* Node.js (at least 6.9.x)
* Angular CLI (via npm: run **npm install -g @angular/cli**)

### Installing

To start backend, run
```
mvn spring-boot:run
```

To start frontend, run
```
ng serve --proxy-config proxy-conf.json
```
or
```
npm start
```

## Authors

Magda Hrebecka

## License

This project is licensed under the MIT License.

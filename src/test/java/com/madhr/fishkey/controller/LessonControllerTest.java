package com.madhr.fishkey.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.madhr.fishkey.FishKeyApplication;
import com.madhr.fishkey.domain.Lesson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
/**
 * Created by Magda on 20.08.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FishKeyApplication.class)
@WebAppConfiguration
public class LessonControllerTest {

    private MockMvc mockMvc;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void when_saveLesson_expect_statusIsOK() throws Exception {
        byte[] lessonJson = toJson(mockLesson());
        mockMvc.perform(post("/api/lessons/save")
                .content(lessonJson)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("mock lesson")));
    }

    @Test
    public void when_getLesson_expect_statusIsOK() throws Exception {
        mockMvc.perform(get("/api/lessons/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)));
    }

    @Test
    public void when_deleteLesson_expect_statusIsOK() throws Exception {
        mockMvc.perform(delete("/api/lessons/delete/{id}", 3L))
                .andExpect(status().isOk());
    }

    @Test
    public void when_getLesson_expect_doesNotExist() throws Exception {
        mockMvc.perform(get("/api/lessons/{id}", 102L))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").doesNotExist());
    }

    private Lesson mockLesson(){
        Lesson lesson = new Lesson();
        lesson.setName("mock lesson");
        return lesson;
    }

    private byte[] toJson(Object r) throws Exception {
        ObjectMapper map = new ObjectMapper();
        return map.writeValueAsString(r).getBytes();
    }
}
package com.madhr.fishkey.repository;

import com.madhr.fishkey.domain.Lesson;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Magda on 17.08.2017.
 */
@Repository
public interface LessonRepository extends CrudRepository<Lesson,Long> {
}

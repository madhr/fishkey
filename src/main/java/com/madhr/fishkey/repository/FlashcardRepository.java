package com.madhr.fishkey.repository;

import com.madhr.fishkey.domain.Flashcard;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Magda on 06.08.2017.
 */
@Repository
public interface FlashcardRepository extends CrudRepository<Flashcard,Long> {
}

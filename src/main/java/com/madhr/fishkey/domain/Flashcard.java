package com.madhr.fishkey.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;

/**
 * Created by Magda on 06.08.2017.
 */

@Entity
@Data
@AllArgsConstructor
public class Flashcard {

    @Id
    @GeneratedValue
    private Long id;
    private String word;
    private String translation;

    @ManyToOne
    private Category category;

    @ManyToOne
    private Lesson lesson;

    public Flashcard(){}

}

package com.madhr.fishkey.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Magda on 17.08.2017.
 */
@Entity
@Data
@AllArgsConstructor
public class Lesson {

    @Id
    @GeneratedValue
    private Long id;
    private String name;

    public Lesson(){}
}

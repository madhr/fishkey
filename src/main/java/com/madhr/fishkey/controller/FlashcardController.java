package com.madhr.fishkey.controller;

import com.madhr.fishkey.domain.Flashcard;
import com.madhr.fishkey.service.FlashcardService;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Magda on 06.08.2017.
 */
@RestController
@RequestMapping("/api/flashcards")
public class FlashcardController {

    private FlashcardService flashcardService;

    public FlashcardController(FlashcardService flashcardService) { this.flashcardService = flashcardService; }

    @GetMapping(value = {"", "/"})
    public Iterable<Flashcard> listFlashcards(){ return flashcardService.list(); }

    @GetMapping(value = "/{id}")
    public Flashcard getFlashcardById(@PathVariable Long id){ return flashcardService.getFlashcardById(id); }

    @PostMapping("/save")
    public Flashcard saveFlashcard(@RequestBody Flashcard flashcard){ return flashcardService.save(flashcard); }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteFlashcard(@PathVariable Long id){ flashcardService.delete(id); }

    @PutMapping(value = "/update/{id}")
    public Flashcard updateFlashcard(@RequestBody Flashcard flashcard, @PathVariable Long id){
        return flashcardService.save(flashcard); }

}

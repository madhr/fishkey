package com.madhr.fishkey.controller;

import com.madhr.fishkey.domain.Category;
import com.madhr.fishkey.service.CategoryService;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Magda on 06.08.2017.
 */
@RestController
@RequestMapping("/api/categories")
public class CategoryController {

    private CategoryService categoryService;

    public CategoryController(CategoryService categoryService) { this.categoryService = categoryService; }

    @GetMapping(value = {"", "/"})
    public Iterable<Category> listCategories(){ return categoryService.list(); }

    @GetMapping(value = "/{id}")
    public Category getCategoryById(@PathVariable Long id){ return categoryService.getCategoryById(id); }

    @PostMapping("/save")
    public Category saveCategory(@RequestBody Category category){ return categoryService.save(category); }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteCategory(@PathVariable Long id){ categoryService.delete(id); }

    @PutMapping(value = "/update")
    public Category updateCategory(@RequestBody Category category){ return categoryService.save(category); }

}

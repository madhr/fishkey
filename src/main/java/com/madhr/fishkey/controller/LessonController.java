package com.madhr.fishkey.controller;

import com.madhr.fishkey.domain.Lesson;
import com.madhr.fishkey.service.LessonService;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Magda on 17.08.2017.
 */
@RestController
@RequestMapping("/api/lessons")
public class LessonController {

    private LessonService lessonService;

    public LessonController(LessonService lessonService) { this.lessonService = lessonService; }

    @GetMapping(value = {"", "/"})
    public Iterable<Lesson> listCategories(){ return lessonService.list(); }

    @GetMapping(value = "/{id}")
    public Lesson getLessonById(@PathVariable Long id){ return lessonService.getLessonById(id); }

    @PostMapping("/save")
    public Lesson saveLesson(@RequestBody Lesson lesson){ return lessonService.save(lesson); }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteLesson(@PathVariable Long id){ lessonService.delete(id); }

    @PutMapping(value = "/update")
    public Lesson updateLesson(@RequestBody Lesson lesson){ return lessonService.save(lesson); }

}

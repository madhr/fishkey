package com.madhr.fishkey;

import com.madhr.fishkey.domain.Category;
import com.madhr.fishkey.domain.Lesson;
import com.madhr.fishkey.domain.Flashcard;
import com.madhr.fishkey.service.CategoryService;
import com.madhr.fishkey.service.LessonService;
import com.madhr.fishkey.service.FlashcardService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;

@SpringBootApplication
public class FishKeyApplication {

	public static void main(String[] args) {
		SpringApplication.run(FishKeyApplication.class, args);
	}

	Category category1 = new Category(1L, "super category");
	Category category2 = new Category(2L, "category");
	Category category3 = new Category(3L, "real category");
	Lesson lesson1 = new Lesson(1L, "IELTS 1");
	Lesson lesson2 = new Lesson(2L, "TOEFL 1");
	Lesson lesson3 = new Lesson(3L, "TOEFL 2");

	@Bean
	CommandLineRunner insertData(CategoryService categoryService, FlashcardService flashcardService, LessonService lessonService){
		return args -> {
			categoryService.save(category1);
			categoryService.save(category2);
			categoryService.save(category3);
			lessonService.save(lesson1);
			lessonService.save(lesson2);
			lessonService.save(lesson3);
			flashcardService.save(new Flashcard(1L, "czerwony", "red", category1, lesson1));
			flashcardService.save(new Flashcard(2L, "zielony", "green", category2, lesson2));
			flashcardService.save(new Flashcard(3L, "niebieski", "blue", category1, lesson2));
		};
	}

}

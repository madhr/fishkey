package com.madhr.fishkey.service;

import com.madhr.fishkey.domain.Flashcard;

/**
 * Created by Magda on 06.08.2017.
 */
public interface FlashcardService {

    Iterable<Flashcard> list();

    Flashcard save(Flashcard flashcard);

    void delete(Long id);

    Flashcard getFlashcardById(Long id);
}

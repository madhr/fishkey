package com.madhr.fishkey.service;

import com.madhr.fishkey.domain.Flashcard;
import com.madhr.fishkey.repository.FlashcardRepository;
import org.springframework.stereotype.Service;

/**
 * Created by Magda on 06.08.2017.
 */
@Service
public class FlashcardServiceImpl implements FlashcardService {

    private FlashcardRepository categoryRepository;

    public FlashcardServiceImpl(FlashcardRepository categoryRepository){ this.categoryRepository = categoryRepository; }

    @Override
    public Iterable<Flashcard> list() { return categoryRepository.findAll(); }

    @Override
    public Flashcard save(Flashcard flashcard) { return categoryRepository.save(flashcard); }

    @Override
    public void delete(Long id) { categoryRepository.delete(id); }

    @Override
    public Flashcard getFlashcardById(Long id) { return categoryRepository.findOne(id); }
}

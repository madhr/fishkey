package com.madhr.fishkey.service;

import com.madhr.fishkey.domain.Lesson;
import com.madhr.fishkey.repository.LessonRepository;
import org.springframework.stereotype.Service;

/**
 * Created by Magda on 17.08.2017.
 */
@Service
public class LessonServiceImpl implements LessonService {

    private LessonRepository lessonRepository;

    public LessonServiceImpl(LessonRepository lessonRepository){ this.lessonRepository = lessonRepository; }

    @Override
    public Iterable<Lesson> list() { return lessonRepository.findAll(); }

    @Override
    public Lesson save(Lesson lesson) { return lessonRepository.save(lesson); }

    @Override
    public void delete(Long id) { lessonRepository.delete(id); }

    @Override
    public Lesson getLessonById(Long id) { return lessonRepository.findOne(id); }
}

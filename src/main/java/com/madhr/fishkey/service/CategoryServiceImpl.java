package com.madhr.fishkey.service;

import com.madhr.fishkey.domain.Category;
import com.madhr.fishkey.repository.CategoryRepository;
import org.springframework.stereotype.Service;

/**
 * Created by Magda on 06.08.2017.
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository){ this.categoryRepository = categoryRepository; }

    @Override
    public Iterable<Category> list() { return categoryRepository.findAll(); }

    @Override
    public Category save(Category category) { return categoryRepository.save(category); }

    @Override
    public void delete(Long id) { categoryRepository.delete(id); }

    @Override
    public Category getCategoryById(Long id) { return categoryRepository.findOne(id); }
}

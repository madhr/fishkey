package com.madhr.fishkey.service;

import com.madhr.fishkey.domain.Lesson;

/**
 * Created by Magda on 17.08.2017.
 */
public interface LessonService {

    Iterable<Lesson> list();

    Lesson save(Lesson lesson);

    void delete(Long id);

    Lesson getLessonById(Long id);
}

package com.madhr.fishkey.service;

import com.madhr.fishkey.domain.Category;

/**
 * Created by Magda on 06.08.2017.
 */
public interface CategoryService {

    Iterable<Category> list();

    Category save(Category category);

    void delete(Long id);

    Category getCategoryById(Long id);
}

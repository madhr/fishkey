import { Component, OnInit } from '@angular/core';
import {Response} from "@angular/http";
import {Lesson} from "./lesson.model";
import {LessonService} from "./lesson.service";

@Component({
  selector: 'app-lesson',
  templateUrl: './lesson.component.html',
  styleUrls: ['./lesson.component.css']
})
export class LessonComponent implements OnInit {

  lessons: Lesson[] = [];
  public lesson: Lesson;
  isCreate = false;
  isUpdate = false;
  isCancelButton = false;
  isAddButton = true;

  constructor(private lessonService: LessonService) { }

  ngOnInit() {
    this.loadLessons();
    this.lessonService.onLessonAdded.subscribe(
      (lesson: Lesson) => this.lessons.push(lesson)
    );

  }

  onCreate(){
    this.isCreate = true;
    this.isCancelButton = true;
    this.isUpdate = false;
    this.isAddButton = false;
    this.lesson = <Lesson>{
      name: ''
    };
    console.log(this.lesson);
  }

  save(model: Lesson, isValid: boolean) {
    let lesson: Lesson = new Lesson(model.name);
    this.lessonService.addLesson(lesson)
      .subscribe(
        (newLesson: Lesson) => {
          this.lessonService.onLessonAdded.emit(newLesson);
        }
      );
    this.isCreate = false;
    this.isAddButton = true;
    this.isCancelButton = false;
  }

  onUpdate(lesson: Lesson){
    this.isUpdate = true;
    this.isCancelButton = true;
    this.isCreate = false;
    this.isAddButton = false;
    this.lesson = <Lesson>{
      id: lesson.id,
      name: lesson.name
    };
  }

  update(model: Lesson, isValid: boolean){
    model.id = this.lesson.id;
    this.lessonService.updateLesson(model)
      .subscribe(
        (model: any[]) => {
          this.loadLessons();
        },
        (error) => console.log(error)
      );
    this.isUpdate = false;
    this.isAddButton = true;
    this.isCancelButton = false;
  }

  onDelete(lesson: Lesson){
    this.lessonService.deleteLesson(lesson).subscribe();
    this.lessonService.getLessons()
      .subscribe(
        (lessons: any[]) => {
          this.lessons = lessons
        },
        (error) => console.log(error)
      );
  }

  onCancel(){
    this.isUpdate = false;
    this.isCreate = false;
    this.isCancelButton = false;
    this.isAddButton = true;
  }

  loadLessons(){
    this.lessonService.getLessons()
      .subscribe(
        (lessons: any[]) => {
          this.lessons = lessons
        },
        (error) => console.log(error)
      );
  }

}

import {Http, Response} from "@angular/http";
import "rxjs/Rx";
import {Lesson} from "./lesson.model";
import {EventEmitter, Injectable} from "@angular/core";

@Injectable()
export class LessonService {

  onLessonAdded = new EventEmitter<Lesson>();

  constructor(private http: Http){}

  getLessons(){
    return this.http.get('/api/lessons')
      .map(
        (response: Response) => {
          return response.json();
        }
      );
  }

  getLessonById(id: number){
    return this.http.get('/api/lessons/'+id)
      .map(
        (response: Response) => {
          return response.json();
        }
      );
  }

  addLesson(lesson: Lesson){
    return this.http.post('api/lessons/save', lesson)
      .map(
        (response: Response) => {
          return response.json();
        }
      );
  }

  saveLesson(lesson: Lesson, checked: boolean) {
    return this.http.post('/api/lessons/save', lesson)
      .map(
        (response: Response) => {
          return response.json();
        }
      )
  }

  updateLesson(lesson: Lesson) {
    return this.http.put('/api/lessons/update/'+lesson.id, lesson)
      .map(
        (response: Response) => {
          return response.json();
        }
      )
  }

  deleteLesson(lesson: Lesson){
    return this.http.delete('/api/lessons/delete/'+lesson.id);
  }
}

import { Component, OnInit } from '@angular/core';
import {Response} from "@angular/http";
import {Category} from "./category.model";
import {CategoryService} from "./category.service";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  categories: Category[] = [];
  public category: Category;
  isCreate = false;
  isUpdate = false;
  isCancelButton = false;
  isAddButton = true;

  constructor(private categoryService: CategoryService) { }

  ngOnInit() {
    this.loadCategories();
    this.categoryService.onCategoryAdded.subscribe(
      (category: Category) => this.categories.push(category)
    );

  }

  onCreate(){
    this.isCreate = true;
    this.isCancelButton = true;
    this.isUpdate = false;
    this.isAddButton = false;
    this.category = <Category>{
      name: ''
    };
    console.log(this.category);
  }

  save(model: Category, isValid: boolean) {
    let category: Category = new Category(model.name);
    this.categoryService.addCategory(category)
      .subscribe(
        (newCategory: Category) => {
          this.categoryService.onCategoryAdded.emit(newCategory);
        }
      );
    this.isCreate = false;
    this.isAddButton = true;
    this.isCancelButton = false;
  }

  onUpdate(category: Category){
    this.isUpdate = true;
    this.isCancelButton = true;
    this.isCreate = false;
    this.isAddButton = false;
    this.category = <Category>{
      id: category.id,
      name: category.name
    };
  }

  update(model: Category, isValid: boolean){
    model.id = this.category.id;
    this.categoryService.updateCategory(model)
      .subscribe(
        (model: any[]) => {
          this.loadCategories();
        },
        (error) => console.log(error)
      );
    this.isUpdate = false;
    this.isAddButton = true;
    this.isCancelButton = false;
  }

  onDelete(category: Category){
    this.categoryService.deleteCategory(category).subscribe();
    this.categoryService.getCategories()
      .subscribe(
        (categories: any[]) => {
          this.categories = categories
        },
        (error) => console.log(error)
      );
  }

  onCancel(){
    this.isUpdate = false;
    this.isCreate = false;
    this.isCancelButton = false;
    this.isAddButton = true;
  }

  loadCategories(){
    this.categoryService.getCategories()
      .subscribe(
        (categories: any[]) => {
          this.categories = categories
        },
        (error) => console.log(error)
      );
  }

}

import {Http, Response} from "@angular/http";
import "rxjs/Rx";
import {Category} from "./category.model";
import {EventEmitter, Injectable} from "@angular/core";

@Injectable()
export class CategoryService {

  onCategoryAdded = new EventEmitter<Category>();

  constructor(private http: Http){}

  getCategories(){
    return this.http.get('/api/categories')
      .map(
        (response: Response) => {
          return response.json();
        }
      );
  }

  getCategoryById(id: number){
    return this.http.get('/api/categories/'+id)
      .map(
        (response: Response) => {
          return response.json();
        }
      );
  }

  addCategory(category: Category){
    return this.http.post('api/categories/save', category)
      .map(
        (response: Response) => {
          return response.json();
        }
      );
  }

  saveCategory(category: Category, checked: boolean) {
    return this.http.post('/api/categories/save', category)
      .map(
        (response: Response) => {
          return response.json();
        }
      )
  }

  updateCategory(category: Category) {
    return this.http.put('/api/categories/update/'+category.id, category)
      .map(
        (response: Response) => {
          return response.json();
        }
      )
  }

  deleteCategory(category: Category){
    return this.http.delete('/api/categories/delete/'+category.id);
  }
}

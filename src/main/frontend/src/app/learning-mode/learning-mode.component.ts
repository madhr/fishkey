import { Component, OnInit } from '@angular/core';
import {FlashcardService} from "../flashcard/flashcard.service";
import {FlashcardComponent} from "../flashcard/flashcard.component";
import {Flashcard} from "../flashcard/flashcard.model";
import {Category} from "../category/category.model";
import {CategoryService} from "../category/category.service";
import {LessonService} from "../lesson/lesson.service";
import {Lesson} from "../lesson/lesson.model";

@Component({
  selector: 'app-learning-mode',
  templateUrl: './learning-mode.component.html',
  styleUrls: ['./learning-mode.component.css']
})
export class LearningModeComponent implements OnInit {

  constructor(private flashcardService: FlashcardService,
              private categoryService: CategoryService,
              private lessonService: LessonService) { }

  flashcards: Flashcard[] = [];
  categories: Category[] = [];
  lessons:    Lesson[] = [];

  ngOnInit() {
    this.loadFlashcards();
    this.loadCategories();
    this.loadLessons();
  }

  loadFlashcards(){
    this.flashcardService.getFlashcards()
      .subscribe(
        (flashcards: any[]) => {
          this.flashcards = flashcards
        },
        (error) => console.log(error)
      );
  }

  loadCategories() {
    this.categoryService.getCategories()
      .subscribe(
        (categories: any[]) => {
          this.categories = categories
        },
        (error) => console.log(error)
      );
  }

  loadLessons() {
    this.lessonService.getLessons()
      .subscribe(
        (lessons: any[]) => {
          this.lessons = lessons
        },
        (error) => console.log(error)
      );
  }

  getCategorysFlashcards(category: Category): Array<Flashcard> {
    let categorysFlashcards: Array<Flashcard> = [];
    for (var flashcard of this.flashcards){
      if(flashcard.category.id==category.id){
        categorysFlashcards.push(flashcard);
      }
    }
    return categorysFlashcards;
  }

  getLessonsFlashcards(lesson: Lesson): Array<Flashcard> {
    let lessonsFlashcards: Array<Flashcard> = [];
    for (var flashcard of this.flashcards){
      if(flashcard.lesson.id==lesson.id){
        lessonsFlashcards.push(flashcard);
      }
    }
    return lessonsFlashcards;
  }


}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { FlashcardComponent } from './flashcard/flashcard.component';
import {FlashcardService} from "./flashcard/flashcard.service";
import { CategoryComponent } from './category/category.component';
import { LessonComponent } from './lesson/lesson.component';
import {CategoryService} from "./category/category.service";
import {LessonService} from "./lesson/lesson.service";
import { LearningModeComponent } from './learning-mode/learning-mode.component';


@NgModule({
  declarations: [
    AppComponent,
    FlashcardComponent,
    CategoryComponent,
    LessonComponent,
    LearningModeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    TagInputModule,
    BrowserAnimationsModule
  ],
  providers: [FlashcardService, CategoryService, LessonService],
  bootstrap: [AppComponent]
})
export class AppModule { }

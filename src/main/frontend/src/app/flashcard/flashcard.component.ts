import { Component, OnInit } from '@angular/core';
import {Response} from "@angular/http";
import {Flashcard} from "./flashcard.model";
import {FlashcardService} from "./flashcard.service";
import {Category} from "../category/category.model";
import {CategoryService} from "../category/category.service";
import {Lesson} from "../lesson/lesson.model";
import {LessonService} from "../lesson/lesson.service";

@Component({
  selector: 'app-flashcard',
  templateUrl: './flashcard.component.html',
  styleUrls: ['./flashcard.component.css']
})
export class FlashcardComponent implements OnInit {

  flashcards: Flashcard[] = [];
  categories: Category[] = [];
  lessons: Lesson[] = [];
  public flashcard: Flashcard;
  isCreate = false;
  isUpdate = false;
  isCancelButton = false;
  isAddButton = true;

  constructor(private flashcardService: FlashcardService,
              private categoryService: CategoryService,
              private lessonService: LessonService) { }

  ngOnInit() {
    this.loadFlashcards();
    this.loadCategories();
    this.loadLessons();
    this.flashcardService.onFlashcardAdded.subscribe(
      (flashcard: Flashcard) => this.flashcards.push(flashcard)
    );

  }

  onCreate(){
    this.isCreate = true;
    this.isCancelButton = true;
    this.isUpdate = false;
    this.isAddButton = false;
    this.flashcard = <Flashcard>{
      word: '',
      translation: '',
      category: null,
      lesson: null
    };
    console.log(this.flashcard);
  }

  save(model: Flashcard, isValid: boolean) {
    console.log(model.category);
    let flashcard: Flashcard = new Flashcard(model.word,
      model.translation,
      model.category,
      model.lesson);
    this.flashcardService.addFlashcard(flashcard)
      .subscribe(
        (newFlashcard: Flashcard) => {
          this.flashcardService.onFlashcardAdded.emit(newFlashcard);
        }
      );
    this.isCreate = false;
    this.isAddButton = true;
    this.isCancelButton = false;
  }

  onUpdate(flashcard: Flashcard){
    this.isUpdate = true;
    this.isCancelButton = true;
    this.isCreate = false;
    this.isAddButton = false;
    this.flashcard = <Flashcard>{
      id: flashcard.id,
      word: flashcard.word,
      translation: flashcard.translation,
      category: flashcard.category,
      lesson: flashcard.lesson
    };
  }

  update(model: Flashcard, isValid: boolean){
    model.id = this.flashcard.id;
    this.flashcardService.updateFlashcard(model)
      .subscribe(
        (model: any[]) => {
          this.loadFlashcards();
        },
        (error) => console.log(error)
      );
    this.isUpdate = false;
    this.isAddButton = true;
    this.isCancelButton = false;
  }

  onDelete(flashcard: Flashcard){
    this.flashcardService.deleteFlashcard(flashcard).subscribe();
    this.flashcardService.getFlashcards()
      .subscribe(
        (flashcards: any[]) => {
          this.flashcards = flashcards
        },
        (error) => console.log(error)
      );
  }

  onCancel(){
    this.isUpdate = false;
    this.isCreate = false;
    this.isCancelButton = false;
    this.isAddButton = true;
  }

  loadFlashcards(){
    this.flashcardService.getFlashcards()
      .subscribe(
        (flashcards: any[]) => {
          this.flashcards = flashcards
        },
        (error) => console.log(error)
      );
  }

  loadCategories(){
    this.categoryService.getCategories()
      .subscribe(
        (categories: any[]) => {
          this.categories = categories
        },
        (error) => console.log(error)
      );
  }

  loadLessons(){
    this.lessonService.getLessons()
      .subscribe(
        (lessons: any[]) => {
          this.lessons = lessons
        },
        (error) => console.log(error)
      );
  }

}

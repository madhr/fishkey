import {Lesson} from "../lesson/lesson.model";
import {Category} from "../category/category.model";
export class Flashcard {
  public id: number;
  public word: string;
  public translation: string;
  public category: Category;
  public lesson: Lesson;

  constructor(word: string, translation: string, category: Category, lesson: Lesson){
    this.word = word;
    this.translation = translation;
    this.category = category;
    this.lesson = lesson;
  }
}

import {Http, Response} from "@angular/http";
import "rxjs/Rx";
import {Flashcard} from "./flashcard.model";
import {EventEmitter, Injectable} from "@angular/core";

@Injectable()
export class FlashcardService {

  onFlashcardAdded = new EventEmitter<Flashcard>();

  constructor(private http: Http){}

  getFlashcards(){
    return this.http.get('/api/flashcards')
      .map(
        (response: Response) => {
          return response.json();
        }
      );
  }

  getFlashcardById(id: number){
    return this.http.get('/api/flashcards/'+id)
      .map(
        (response: Response) => {
          return response.json();
        }
      );
  }

  addFlashcard(flashcard: Flashcard){
    return this.http.post('api/flashcards/save', flashcard)
      .map(
        (response: Response) => {
          return response.json();
        }
      );
  }

  saveFlashcard(flashcard: Flashcard, checked: boolean) {
    return this.http.post('/api/flashcards/save', flashcard)
      .map(
        (response: Response) => {
          return response.json();
        }
      )
  }

  updateFlashcard(flashcard: Flashcard) {
    return this.http.put('/api/flashcards/update/'+flashcard.id, flashcard)
      .map(
        (response: Response) => {
          return response.json();
        }
      )
  }

  deleteFlashcard(flashcard: Flashcard){
    return this.http.delete('/api/flashcards/delete/'+flashcard.id);
  }
}

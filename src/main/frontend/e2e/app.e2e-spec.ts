import { FishkeyPage } from './app.po';

describe('fishkey App', () => {
  let page: FishkeyPage;

  beforeEach(() => {
    page = new FishkeyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
